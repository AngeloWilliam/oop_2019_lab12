﻿using System;
using System.Collections.Generic;


namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         */ 
        
        private IDictionary<Tuple<TKey1,TKey2>,TValue> values;

        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            foreach(TKey1 k1 in keys1)
            {
                foreach(TKey2 k2 in keys2)
                {
                    values.Add(new Tuple<TKey1, TKey2>(k1,k2),generator(k1,k2));
                }
            }    
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            foreach (Tuple<TKey1, TKey2, TValue> a in this.GetElements())
            {
                foreach (Tuple<TKey1, TKey2, TValue> b in other.GetElements())
                {
                    if(a.Item1.Equals(b.Item1) && a.Item2.Equals(b.Item2) && a.Item3.Equals(b.Item3))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            get
            {
                values.TryGetValue(new Tuple<TKey1, TKey2>(key1, key2), out TValue i);
                return i;
            }

            set
            {
                this.values[new Tuple<TKey1, TKey2>(key1, key2)] = value;
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> res = new List<Tuple<TKey2, TValue>>();

            foreach (var a in this.values)
            {
                if (a.Key.Item1.Equals(key1))
                {
                    res.Add(new Tuple<TKey2, TValue>(a.Key.Item2, a.Value));
                }
            }
                return res;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> res = new List<Tuple<TKey1, TValue>>();

            foreach (var a in this.values)
            {
                if (a.Key.Item2.Equals(key2))
                {
                    res.Add(new Tuple<TKey1, TValue>(a.Key.Item1, a.Value));
                }
            }
            return res;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> res = new List<Tuple<TKey1, TKey2, TValue>>();

            foreach (KeyValuePair<Tuple<TKey1, TKey2>, TValue> a in this.values)
            {
                res.Add(new Tuple<TKey1, TKey2, TValue>(a.Key.Item1, a.Key.Item2, a.Value));
            }
            
            return res;
        }

        public int NumberOfElements
        {
            get
            {
                return this.GetElements().Count;
            }
        }

        public override string ToString()
        {
            String str = "";
            foreach(Tuple<TKey1, TKey2, TValue> a in this.GetElements())
            {
                str += ("Key1: " + a.Item1 + " Key2: " + a.Item2 + " Value: " + a.Item3 + "\n");
            }
            return str;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
